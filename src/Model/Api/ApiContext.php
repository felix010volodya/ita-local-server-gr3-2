<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext
{
    /** @var Curl */
    private $curl;

    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $apiUrl;

    /** @var string */
    private $token;

    const METHOD_GET = 1;
    const METHOD_POST = 2;
    const METHOD_HEAD = 3;
    const METHOD_PUT = 4;
    const METHOD_PATCH = 5;
    const METHOD_DELETE = 6;


    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT_EXISTS = '/client/{passport}/{email}';

    /**
     * ApiContext constructor.
     * @param ContainerInterface $container
     * @throws \ErrorException
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->apiUrl = $this->getParameter('api')['central']['url'];
        $this->token = $this->getParameter('api')['central']['token'];

        $this->curl = new Curl();
        $this->curl->setJsonDecoder(function () {
            $args = func_get_args();

            $response = json_decode($args[0], true);
            if ($response === null) {
                $response = $args[0];
            }
            return $response;
        });
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endpoint = str_replace(
            ['{passport}', '{email}'],
            [$passport, $email],
            self::ENDPOINT_CLIENT_EXISTS
        );


        try {
            $this->makeQuery($endpoint, self::METHOD_HEAD);
            return true;
        } catch (ApiException $e) {
            if($e->getCode()!= 404) {
                throw $e;
            }

            return false;
        }
    }

    /**
     * @param string $endpoint
     * @param string $httpMethod
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    protected function makeQuery(string $endpoint, string $httpMethod, array $data = [])
    {
        $url = $this->apiUrl . $endpoint;
        $data = array_merge(['token' => $this->token], $data);

        switch ($httpMethod) {
            case self::METHOD_GET:
                $this->curl->get($url, $data);
                break;
            case self::METHOD_PUT:
                $this->curl->put($url, $data);
                break;
            case self::METHOD_POST:
                $this->curl->post($url, $data);
                break;
            case self::METHOD_PATCH:
                $this->curl->patch($url, $data);
                break;
            case self::METHOD_HEAD:
                $this->curl->head($url, $data);
                break;
            case self::METHOD_DELETE:
                $this->curl->delete($url, $data);
                break;
            default:
                break;
        }

        if ($this->curl->error) {
            throw new ApiException(
                $this->curl->errorMessage,
                $this->curl->errorCode
            );
        } else {
            return $this->curl->response;
        }
    }

    protected function getParameter(string $name)
    {
        return $this->container->getParameter($name);
    }
}