<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{

    /**
     * @Route("/", name="ping")
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: '.$e->getMessage());
        }
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function authAction(UserRepository $userRepository)
    {
        $user = $userRepository->find(1);
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
        return new Response('Вы авторизованы');
    }

    /**
     * @Route("/qwe", name="qwe")
     */
    public function qweAction()
    {
        return new Response('1231312');
    }
}
